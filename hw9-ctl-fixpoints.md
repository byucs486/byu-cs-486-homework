# Objectives

  * Create an expression tree from a CTL expression
  * Convert a CTL formula to an equivalent formula that only uses `EX`, `EG`, and `EU` and boolean connectives via De Morgan's law
  * Compute `EG` and `EU` via greatest and least fix-points
  * Verify CTL properties on simple models by hand using fix-points

# Reading

See [ctl-mc-fix-points.md](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/ctl-mc-fix-points.md)

# Problems

  1. **(16 points)** Draw the expression tree for the following CTL formula: `EX a ∧ E(b U EG ¬c)`
  2. **(10 points)** Write the equivalent formula for the one presented that only uses EX, EU, and EG for path-temporal operators: `AG(a → AF(b ∧ ¬a)`). Show any intermediary steps.
  3. **(10 points)** Write the equivalent formula for the one presented that only uses EX, EU, and EG for path-temporal operators: `AX(E(¬a U (b ∧ c)) ∨ EG AX a)`. Show any intermediary steps.
  4. **(36 points)** Do Exercise 6.1 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 452 of the PDF document). Convert any formula to existential normal form as needed.
  5. **(8 points)** For the tree in problem one, replace any sub-formulas with equivalent least or greatest fix point computations.
  6. **(6 points)** Consider the formula `EG f`.  Draw a transition system for which it takes 4 iterations on the equivalent fix-point calculation to reach the fix-point. Show the labeled set of state on each iteration.
  7. **(6 points)** Consider the formula `EF f`. Draw a transition system for which it takes 5 iterations on the equivalent fix-point calculation to reach the fix-point. Show the labeled set of states on each iteration.