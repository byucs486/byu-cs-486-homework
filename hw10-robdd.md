# Objectives

  * Use the Shannon expansion to create Reduced Ordered Binary Decision Diagrams (ROBDDs) from arbitrary Boolean formulas
  * See how variable ordering affects the size of a resulting ROBDD
  * Use the if-then-else (ITE) algorithm to implement Boolean operations
  * Create new ROBDDs that represent new Boolean functions using ITE

# Reading

  * [ITE.ppt](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/ITE.ppt)
  * [replace.ppt](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/replace.ppt)
  * [relProd.ppt](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/relProd.ppt)

# Problems

  1. **(10 points)** Use the Shannon expansion to create an ROBDD for the predicate `F(a, b, c, d) = (a ∧ b) ∨ (¬a ∧ c) ∨ (b ∧ ¬c ∧ d)` with the following variable order `a < b < c < d`. Show the complete tree from the Shannon expansion, the complete compute (unique) table, and the graphical depiction of the final ROBDD.

  2. **(10 points)** Repeat problem 1 only this time use the variable order `d < c < b < a`.

  3. **(10 points)** Suppose you have ROBDDS, f and g. What is(are) the ITE call(s) to compute the following functions?

    * `f ∧ g`
    * `f ∨ g`
    * `¬f`
    * `¬(f ∧ g)`
    * `f → g`
    * `f ↔ g`

  4. **(20 points)** Repeat problem 1, only this time, rather than use the Shannon expansion directly, use the ITE algorithm. Initialize the unique table with ROBDD nodes for variables `a`, `b`, `c`, and `d`. Show each call to ITE.

  5. **(20 points)** Repeat problem 4, only this time us the variable order `d < c < b < a`. Re-initialize the unique table to reflect this order.

  6. **(20 points)** Suppose you have two additional variables `e` and `f` such at `a < b < c < d < e < f`. Apply the replace algorithm on the BDD from problem 4 such that `c` is replaced with `f` and `d` is replaced with `e`. This method is `BDD::SwapVariables` in the Cudd package. Show the added entries to the unique table, the calls to ITE, and draw the final BDD.