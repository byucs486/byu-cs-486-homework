# Objective

  * Write LTL properties for Promela models
  * Verify LTL properties with spin

# Reading

 * [BYU-Temporal-Logic.ppt](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/BYU-Temporal-Logic.ppt)
 * [Spin manual pages](http://spinroot.com/spin/Man/promela.html)

# Problems

  1. (20 points) Verify with `spin` using `ltl` formulas Lamport's Bakery algorithm for 4 processes (see [HW1](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/hw1-mutual-exclusion-search.md)) against the following properties:
  
    * Only one process is in the critical section at a time (mutual exclusion)
    * There is no deadlock
    * Any process that tries to enter the critical section eventually does

    Be sure to clearly indicate the `ltl` formula for each property.
    
  2. (20 points) Verify each of the properties for the Szymanski's protocol for 4 processes with `spin` using `ltl` (see [HW2](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/hw2-nway-mutex-message-passing.md)). Cleary indicate each `ltl` property.
  3. (20 points) Verify Peterson's leader election algorithm with `spin` using `ltl` (see [HW2](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/hw2-nway-mutex-message-passing.md)). Clearly indicate each `ltl` property.
  4. (20 points) Verify the elevator with `spin` using `ltl` (see [HW4](https://bitbucket.org/byucs486/byu-cs-486-homework/src/master/hw4-safety-NFA-never.md)). Clearly indicate each `ltl` property.
