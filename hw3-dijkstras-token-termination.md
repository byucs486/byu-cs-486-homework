# Objectives

  * Model and verify Dijkstra's distributed token termination algorithm
  * Gain mastery of message passing in `spin`

# Reading

  * [Dijkstra's Token Termination Algorithm](https://www3.risc.jku.at/software/daj/TermDetect/)
  * [Detailed presentation](http://people.cs.aau.dk/~adavid/teaching/MVP-10/17-Termination-lect14.pdf)
  * Another [similar presentation](http://www.thi.informatik.uni-frankfurt.de/lehre/pda/ws0910/pda_ws0910_kapitel07.pdf) to the previous

# Problems

The problems are staged to evolve the model from its most simple form to its most complex form that would more realistically be used in an actual deployment. The expectation is that there is a model for each one.

All the problems but the last ask for a PROMELA model with verification conditions for `spin` to prove that the model is correct. The models should be parameterized by `N`, the number of nodes participating in the system.

The algorithm is correct if none of the processes deadlock, the model terminates given a finite number of work messages, and at termination

  * all the processes are inactive
  * all the message queues are empty
  * all the participating processes are at valid end states (the closing curly brace of the definition)

These can be verified with assertions or never-claims.

A few notes on the algorithm:

  * There are two types of messages in the model: token messages and work messages
  * Only work messages count in the send/receive counters (though not required)
  * A work message causes a inactive node to become active
  * An active nodes holds a token until there are no work messages in its queue at which point it becomes inactive and sends along the token 
  * A single start process produces tokens and consumes tokens
  * If the start process starts active and black, then it may simply become inactive and white to then generate the token.

1. **(30 points)** Create and verify a model where all nodes are inactive (and are always inactive). The token goes exactly one-time around in this model and the system then terminates. The only messages in the system are related to tokens for the algorithm that move around the logical ring of processes. The starting process should only need to send a single white token into the ring, and that token should come back white. The starting process should also send a poison pill (shutdown message) around the ring once it receives back the white token. Processes should exit after receiving and passing on the poison pill message.

2. **(30 points)** Create and verify a model that adds to the previous model the ability for processes to start active with color black (e.g., it has sent or received a message and is now black). Active nodes **do not** send any *work* tokens to other nodes to keep the model simple. An active node switches to inactive and white once it receives a token message. The token message is forwarded along the ring in the appropriate state. The number of active nodes in this model only decreases as the token moves around the ring. If any node starts active with color black, then it should take two tokens to detect termination. Again, as in the previous model, there are no messages from the active nodes to count for the termination condition; the start process only needs to see a white token come back to start termination with the poison pill.

3. **(30 points)** Create and verify a model that adds to the previous model the ability for an active node to send at most one work message over its entire life-cycle, and that message can be sent to any other process in the system. If a inactive node receives a work message causing it to become active and black, then as before, it too may send at most one work message in its lifetime to any other process in the system. In this way, even if a inactive process becomes active and black multiple times, it never sends more than one work message total. As such, there can never be more than `N` work messages ever sent. An active node non-deterministically chooses if it will send its one message or not. In this model, the work message counts are part of the termination condition on the token. 

4. **(10 points)** How big does `N` need to be to verify the algorithm correct for any number of processes. Justify your answer.

# Submission

Upload to learning suite a zip archive with all the models and any other text needed to answer the problems. Include a README to help the TA grade.