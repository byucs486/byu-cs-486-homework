# Objectives

  * Classify properties according to safety and liveness
  * Write NFAs for regular safety properties
  * Verify regular safety properties in SPIN
  * Compute the synchronous intersection of two NFAs

# Reading

  * Section 3.3, 3.4, 4.1, and 4.2 of [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf)
  * [Safety and Liveness](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/safety-liveness.md)
  * [Regular Safety Properties](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/regular-safety-properties/regular-safety-properties.md)

# Problems

  1. **(24 points)** 1. Classify each of the following properties as either safety or liveness. Give an accompanying counter-example to the property. If the property is a safety property, further indicate if it is a regular safety property, and if regular, give the NFA to verify it.

    * The number of items in a buffer may not exceed `n`
    * If a process reaches a location `li` just before a critical section, it should not be blocked forever
    * Clients may not retain resources forever
    * It is always cloudy immediately before it rains
    * The transition from `¬p` to `p` will happen at most once
    * There are infinitely many transitions from `¬p` to `p` (and vice versa)
    * The lights of a traffic signal light in the following periodic sequence: red, yellow, green, red, yellow, green,… and only one light is on at any given time, starting with red.
    * If `q` holds in a state `si` and `r` holds in some subsequent state `(sj, j > i)` then `p` must not hold in any state `sk` in between `si` and `sj` (`i < k < j`, where `j` is the first such subsequent state)

  2. **(40 points)** This problem is Exercise 5.5 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 321 of the PDF document). Consider an elevator system that services `N > 0` floors numbered `0` through `N-1`. There is an elevator door at each floor with a call-button and an indicator light that signals whether or not the elevator has been called. The system must have the following properties:

    * Any door on any floor is never open when the elevator is not present
    * A requested floor will be served eventually
    * The elevator repeatedly returns to floor `0`
    * When the top floor is requested, the elevator serves it immediately and does not stop on any floor on the way up

    Write a PROMELA verification model for the elevator. Identify which properties are safety properties. Verify only the **safety properties** with SPIN using never claims assuming that the bad prefixes can be described by a **regular language**.

  3. **(16 points)** Do Exercise 4.5 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 236 of the PDF document). The question asks to compute the intersection of an Automaton with a labeled transition system. Show the automaton for the resulting intersection. Is the language empty?

  4. **(20 points)** Do Exercise 4.6 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 236 of the PDF document. The question asks to compute `Psafe` and the intersection with the NFA for the system. `Psafe` is a safety property that should hold. Negate it to create the never-claim for the intersection (e.g., the bad prefixes).