# Objectives

  * Download and install SPIN
  * Create processes in SPIN
  * Use non-determinism
  * Control processes to run in specific orders
  * Learn `_pid` and `_nr_pr` variables

# Reading

  * [Install from SPIN lecture notes](https://bitbucket.org/byucs329/byu-cs-486-lecture-notes/src/master/spin/install.md)
  * [SPIN lecture notes](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/spin/spin.md)

# Problems

  1. (**5 points**) Download and install the [http://spinroot.com SPIN model checker]. See . Submit a screen shot from the command '''spin -?'''
  2. (**20 points**) Write a PROMELA model that starts no more than `N` processes randomly where `N` is given with a `#define`. Here the `init` process is not part of `N`. Each of the `N` processes should output its process ID in the order of creation. After that, each process should then output its process ID in reverse order of creation, and then finally **die** before the next process reports its ID and dies. Report the number of states found for `N = 5` in verification mode.
  3. (**10 points**) Try a variant of the previous model where the `N` processes **die** in the actual order of creation. Explain why it is not possible to write this variant of the model. Create a similar model where the processes **terminate** in the order of creation rather than **die**.

# Notes:

Only the for-statement, run-statement, `_nr_pr`, and `_pid` constructs with some local and global variables are required for problems (2) and (3). **Do not use any other control flow such as do-statements or if-statements**.

Problem (3) cannot be solved using `_nr_pr` as it only changes value when a process *dies* meaning that it is removed from the system.

# Submission

Upload the final files in a `zip`-archive.