# Objectives

  * Write simple PROMELA verification models that use shared variables for synchronization
  * Verify mutual exclusion algorithms with assert-statements **and** never-claims
    1. Mutual exclusion on the critical section
    2. No deadlock
    3. It works if only one process is active (no need to strictly alternate)
  * Use PROMELA to spin's search ability to solve a puzzle

# Problems

1. **(30 points)** Write a Promela model for the below problem **(25 point)** and use SPIN to find a solution **(5 points)**. *A farmer wants to cross a river and take with him a wolf, a goat, and a cabbage. There is a boat that can fit himself plus either the wolf, the goat, or the cabbage. If the wolf and the goat are alone on one shore, the wolf will eat the goat. If the goat and the cabbage are alone on the shore, the goat will eat the cabbage. How can the farmer bring the wolf, the goat, and the cabbage across the river?*

2. **(30 points)** Model Hyman's mutual exclusion algorithm with PROMELA **(25 points)** and prove its correctness or incorrectness with SPIN **(5 points)** using assertions.

```java
void lock (int i) {
  int j = 1 - i;
  signal[i] = 1;
  while (turn != i) {
    while (signal[j]);
    turn = i;
  }
}
 
void unlock(int i) {
  signal[i] = 0;
}
```

3. **(30 points)** Model Peterson's algorithm for mutual exclusion below with PROMELA **(25 points)** and prove its correctness or incorrectness with SPIN **(5 points)** using never-claims.

```java
public void lock() {
 flag[i] = true; 
 victim  = i; 
 while (flag[j] && victim == i) {};
}
public void unlock() {
 flag[i] = false;
}
```

4. **(30 points)** Model the Lamport's Bakery Algorithm for mutual exclusion below with PROMELA **(25 points)** and prove its correctness or incorrectness with SPIN **(5 points)**. Make it parameterized on `N` the number of participating processes. Verify it up to `N = 4`. Does adding more processes change the result? Why?

```java
class Bakery implements Lock {
  volatile boolean[] flag;
  volatile Label[] label;
  public Bakery (int n) {
    flag  = new boolean[n];
    label = new Label[n];
    for (int i = 0; i < n; i++) { 
       flag[i] = false; label[i] = 0;
    }
  }

  public void lock() {  
    flag[i]  = true;  
    label[i] = max(label[0], …,label[n-1])+1;
    while (exists k such that flag[k]
           && (label[i],i) > (label[k],k));
   }

  public void unlock() {  
   flag[i] = false;
  }
}
```

The meaning of `(label[i],i) > (label[k],k)` is a lexicographic compare. It first compares `label[i] > label[k]` using the first entry in the tuple as the primary key. If the labels are not equal, then the truth value of the compare is known. If however the labels are equal, then `i > k` is evaluated. In this way, the second entry in the tuple is the secondary key: `(label[i] > label[k]) || (label[i] == label[k] && i > k)`.

# Submission

Upload to learning suite a zip archive with all the models. Include a README to help the TA grade.