# Objectives

  * Model and verify Szpmanski's n-way mutual exclusion algorithm (also referred to as the filter algorithm)
  * Use message passing to complete and verify a producer consumer model
  * Model and verify Peterson's algorithm for leader election
  
# Problems

**1.** (40 points) This problem is [Exercise 5.25 in Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 327 of the PDF document). Consider an arbitrary, but finite, number of identical processes, that execute in parallel. Each process consists of a noncritical part and a critical part, usually called the critical section. In this exercise we are concerned with the verification of a mutual exclusion protocol, that is, a protocol that should ensure that at any moment of time at most one process (among the `N` processes in our configuration) is in its critical section. In this exercise we are concerned with Szymanski’s protocol. Assume there are `N` processes for some fixed `N > 0`. There is a global variable, referred to as `flag`, which is an array of length `N`, such that `flag[i]` is a value between `0` and 4 for each process `i` also between `0` and `N-1`. The idea is that `flag[i]` indicates the status of process `i`. The protocol executed by process `i` looks as follows:

```
l0: loop forever do begin
l1: Noncritical section 
l2: flag[i] := 1;
l3: wait until (flag[0] < 3 and flag[1] < 3 and . . . and flag[N-1] < 3) 
l4: flag[i] := 3;
l5: if (flag[0]=1 or flag[1]=1 or ... or flag[N-1]=1)
     then begin
        l6: flag[i] := 2;
        l7: wait until (flag[0] = 4 or flag[1] = 4 or ... or flag[N-1] = 4) 
     end
l8: flag[i] := 4;
l9: wait until (flag[0] < 2 and flag[1] < 2 and ... and flag[i-1] < 2)
l10: Critical section
l11: wait until (flag[i+1] ∈ {0,1,4}) and ... and (flag[N-1] ∈ {0,1,4}) 
l12: flag[i] := 0;
end.
```

Before doing any of the exercises listed below, try first to informally understand what the protocol is doing and why it could be correct in the sense that mutual exclusion is ensured. If you are convinced of the fact that the correctness of this protocol is not easy to see (otherwise please inform me) then start with the following questions.

  * Model Szymanski’s protocol in Promela. Assume that all tests on the global variable `flag` (such as the one in statement `l3`) are atomic. Look carefully at the indices of the variable `flag` used in the tests. Make the protocol description modular such that the number of processes can be changed easily.
  * Check for several values of `N` (`N >= 2`) that the protocol indeed ensures mutual exclusion. Report your results for `N = 4`.
  * The code that a process has to go through before reaching the critical section can be divided into several segments. We refer to statement `l4` as the **doorway**, to segments `l5`, `l6`, and `l7`, as the **waiting room** and to segments `l8` through `l12` (which contains the critical section) as the **inner sanctum**. You are requested to check the following claims. Give for each case the changes to your original Promela specification for Szymanski’s protocol and present the verification results. In case of negative results, explain the counter-example. Prove the following properties for `N = 4` only -- it is OK to write the properties to only work for `N == 4`

    1. Whenever some process is in the **inner sanctum**, the **doorway** is locked, that is, no process is at location `l4`.
    2. If a process `i` is at `l10`, `l11` or `l12`, then it has the least index of all the processes in the **waiting room** and the **inner sanctum**.
    3. If some process is at `l12`, then all processes in the **waiting room** and in the **inner sanctum** must have flag value `4`.

**2.** (20 points) This problem comes from [CalTech's CS 118 Assignment 4](https://d1b10bmlvqabco.cloudfront.net/attach/i4libjkuz9io1/i4licnd7ihy2nh/i5zqbdqd68yu/Assignment_4_CS118_2015.pdf). Consider a distributed system that consists of five producers, one scheduler, and two consumers. Each producer has two tasks that need to be completed by a consumer. For each task, a producer sends a request to the scheduler. The scheduler has a queue to store up to five incoming producer requests. The scheduler, upon receiving a request, assigns the task to one of the consumers. The two consumers have their own separate queue of outstanding tasks. A consumer can only service one request at a time, but it can have up to two outstanding tasks waiting in the queue. Consumers do not acknowledge completion of tasks.

The definitions for the processes for `Producer`, `Consumer`, and `init` are provided below. Write the definition for `Scheduler` for assigning tasks to the consumers. The scheduler is constrained by the following property when assigning tasks: it must balance the workloads of the two consumers. That is, a new task is always assigned to the consumer who has the fewest number of outstanding tasks in its queue. If the number of tasks currently assigned is equal, then the task could be assigned to either consumer. Prove with SPIN that the solution has the right properties.

```
mtype = { task }
chan prodToSched = [5] of {mtype, byte}
chan schedToCons[2] = [10] of {mtype, byte}
 
proctype Producer(byte me) {
  prodToSched!task(me)
  prodToSched!task(me)
}
 
proctype Consumer(byte me) { 
  byte you
  do
  :: schedToCons[me]?task(you)
  :: timeout -> break
  od
}
 
init { 
  byte i
  atomic { 
    for (i : 0 .. 4) {
      run Producer(i)
    }
    run Consumer(0)
    run Consumer(1)
  } 
}
 
active proctype Scheduler () {  
  /* you write this */
}
```

**3.** (40 points) This problem is [Exercise 5.26 in Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf) (page 328 of the PDF document). We assume `N` processes in a ring topology, connected by unbounded queues. A process can only send messages in a clockwise manner. Initially, each process has a unique identifier `ident` (which is assumed to be a natural number). A process can be either active or relaying. Initially a process is active. In Peterson’s leader election algorithm (1982) each process in the ring carries out the following task:

```
active:
d := ident; 
do forever 
begin
  /* start phase */
  send(d);
  receive(e);
  if e = ident then announce elected; 
  if d > e then send(d) else send(e); 
  receive(f);
  if f = ident then announce elected;
  if e >= max(d,f) then d := e else goto relay; 
end
 
relay:
do forever 
begin
  receive(d);
  if d = ident then announce elected;
  send(d)
end
```

Model Peterson’s leader election protocol in Promela (avoiding invalid end states) and verify any of the following properties that are **regular safety properties**:

  1. There is always at most one leader.
  2. Eventually always a leader will be elected.
  3. The elected leader will be the process with the highest number.

# Submission

Upload to learning suite a zip archive with all the models. Include a README to help the TA grade.