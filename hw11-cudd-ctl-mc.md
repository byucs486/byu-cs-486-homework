# Objectives

  * Implement EU and EG using fix-points
  * Model simple C-programs with BDDs
  * Do symbolic CTL model checking
# Tools

## Cudd

The easiest way to use Cudd is with [cudd.tgz](https://students.cs.byu.edu/~egm/downloads/cudd.tgz) which is an archive with the the x86-64 compiled libraries, headers, makefile, and example code for the CS Open lab machines.

[GitHub CUDD 3.0.0](https://github.com/ivmai/cudd) snapshot is the easiest way to obtain CUDD.

For those using a generic Ubuntu docker image:

```bash
apt-get update
apt-get install -y git g++ make autoconf automake build-essential libtool
git clone https://github.com/ivmai/cudd
cd cudd
autoreconf -vfi
./configure
make
```

For the OS X build, I did the following:

```bash
$ ./configure CC=clang CXX=clang++
$ make
...
$ make check
```

All the tests should pass.

Since most likely you do _not_ have install privileges, then you can do the following to add the header and libs into single directories.

```bash
$ cd cudd-3.0.0
$ mkdir lib
$ cd lib
$ ln -s ../cudd/.libs/libcudd.a .
$ ln -s ../cplusplus/.libs/libobj.a .
$ cd ..
$ mkdir include
$ cd include
$ ln -s ../cplusplus/cuddObj.hh .
$ ln -s ../cudd/cudd.h .
```

You should be able to use -I and -L to set the include and library path to CUDD.

```bash
$ clang++ -g -Wall -D USE_CUDD -std=c++0x -I/Users/egm/Documents/cudd-3.0.0/include -c mutex-one-bdd.cc
$ clang++ -g -Wall -D USE_CUDD -std=c++0x mutex-one-bdd.o -o mutex-one-bdd -L/Users/egm/Documents/cudd-3.0.0/lib -lobj -lcudd
If you are having linking errors, then be sure you are using the same std library as CUDD is using in its build.
```

## Graphviz

Install Graphviz to visualize BDDs. Once it is installed, `Cudd::DumpDot` will generate DOT files. These files are converted to PDF with

```bash
$ dot -Tpdf -o file.pdf file.dot
```

Please note that CUDD uses complement edges. These are depicted as doted lines as opposed to long-dash lines. You have to traverse from the label to the root node as that edge may be a complement edge. In the BDD, the complement edge has the same meaning as the dashed edge: set the variable low. Additionally, the complement edge tells you if you need to complement the terminal when you reach the terminal. If the path being considered from the function label includes an ever number of complement edges, then the terminal is unchanged. If the path from the function label being considered includes an odd number of complement edges, then the terminal is complimented (i.e., terminal one with an even number of compliment edges in the path and terminal zero with an odd number of edges in the path).

# Problems

  1) (40 points) Write algorithms to compute EG and EU as fix-point calculations using the Cudd library. Include tests that automatically check the correctness of your implementation. 20 points are allocated to the algorithms and the other 20 points are allocated to the quality of the tests.

  2) (40 points) Verify the Hyman's Mutual Exclusion protocol below for two threads using the EG and EU code from the first problem. Check for mutual exclusion (e.g., there is no path where the protocol does not work) and the absence of starvation (e.g., every request for the critical section is eventually granted). Be sure to clearly indicate the CTL formulas for each property.

    Hyman's Mutual Exclusion

```java
    void lock (int i) {
      int j = 1 - i;
      signal[i] = 1;
      while (turn != i) {
          while (signal[j]);
          turn = i;
      }
    }

    void unlock(int i) {
      signal[i] = 0;
    }
```

  3) (40 points) Verify the Peterson's mutual exclusion protocol below for two threads using the EG and EU code from the first problem. Check for mutual exclusion (e.g., there is no path where the protocol does not work) and the absence of starvation (e.g., every request for the critical section is eventually granted). Be sure to clearly indicate the CTL formulas for each property.

    Peterson's Mutual Exclusion

```java
    void lock (int i) {
      int j = 1 - i;
      signal[i] = 1;
      turn = j;
      while (signal[j] && turn != i);
    }

    void unlock(int i) {
      signal[i] = 0;
    }
```