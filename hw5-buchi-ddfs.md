# Objectives

  * Reason about omega-regular expressions
  * Design NBA (non-deterministic buichi automaton) for given specifications
  * Compute the synchronous product of NBAs
  * Perform double depth-first search (DDFS) on a given NBA
  * Compute the synchronous product and DDFS *on-the-fly* (i.e., at the same time) given two NBAs
  * 
# Reading

  * See [buichi-verification.md](https://bitbucket.org/byucs486/byu-cs-486-lecture-notes/src/master/buichi-verification/buchi-verification.md)

# Problems

  1. **(16 points)** This problem is Exercise 4.7 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf). Prove or disprove the following equivalences for omega-regular expressions. Note that the `.`-operator is concatenation, the `+`-operator is union (e.g., or), the carrot denotes superscript, `w` is a stand-in for omega (infinite repetition), and `*` is the Kleene star.

    * `(E1 + E2).F^w = E1.F^w + E2.F^w`
    * `E.(F1 + F2)^w = E.F1^w + E.F2^w`
    * `E.(F.F^*)^w = E.F^w`
    * `(E^*.F)^w = E^*.F^w`

  2. **(10 points)** This problem is Exercise 4.9 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf). Let alphabet be `{A,B}`. Construct an NBA that accepts the set of infinite words over the alphabet such that `A` occurs infinitely many times in the word and between any two successive `A`'s an odd number of `B`'s occur.

  3. **(10 points)** This problem is Exercise 4.11 in [Principles of Model Checking](http://is.ifmo.ru/books/_principles_of_model_checking.pdf). Give an NBA for the omega-regular expression `(AB + C)^*((AA + B)C)^w + (A^*C)^w`.

  4. **(16 points)** Compute the full intersection for the two NBAs in [Figure 1](hw5-figures.pdf). Be sure to clearly indicate accept states.

  5. **(20 points)** Compute the full intersection for the two NBAs in [Figure 2](hw5-figures.pdf). Be sure to clearly indicate accept states.

  6. **(20 points)** Apply double-depth-first search to the automaton from the intersection in Problem 4 to detect a cycle in the intersection. Always visit the 0-input edge first. Assign each state a pre-order visit number. States that are visited twice (once if the first search and again in the second search) should have two pre-order visit numbers. If a cycle is detected, then clearly indicate that cycle. Grading is on the pre-order visit numbers.

  7. **(20 points)** Repeat Problem 5, only this time use a double-depth-first search to detect a cycle at the same time that the intersection is being computed. This is often referred to as on-the-fly model checking. As in the previous problem, always visit the 0-input edge first. Assign each state a pre-order visit number. States that are visited twice (once if the first search and again in the second search) should have two pre-order visit numbers. If a cycle is detected, then clearly indicate that cycle. Grading is on the pre-order visit numbers. Did the entire intersection get enumerated before a cycle is detected?

  8. **(20 points)** Consider the state space and never-claim in [Figure 5](hw5-figures.pdf). Perform on-the-fly model checking to see if an accepting cycle exists. Visit edge-labels in alphabetical order (e.g., `A`, then `B`, and then `C`). If there is a non-deterministic choice of edges, then visit the edge leading to the least state-label first (e.g., `s3` is visited before `s5`).